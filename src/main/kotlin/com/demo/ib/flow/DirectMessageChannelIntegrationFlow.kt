package com.demo.ib.flow

import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.channel.MessageChannels
import org.springframework.messaging.MessageHandler

//@Configuration
class DirectMessageChannelIntegrationFlow {

//    @Bean
    fun integrationFlow() =
        IntegrationFlows
            .from(MessageChannels.direct("messageChannel"))
            .filter("World"::equals)
            .transform<String, String> {
                "Hello, $it"
            }
            .handle(MessageHandler { println(it.payload) })
            .get()
}