package com.demo.ib.flow

import org.springframework.beans.factory.annotation.Value
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.Pollers
import org.springframework.integration.file.dsl.Files
import org.springframework.messaging.MessageHandler
import java.io.File

//@Configuration
class FileIntegrationFlow {

//    @Bean
    fun integrationFlow(@Value("\${input.folder:/Users/thegreatdao/Desktop/integration}") inputFolder: String) =
        IntegrationFlows
            .from(Files.inboundAdapter(File(inputFolder))) {
                it.poller(Pollers.fixedRate(1000))
            }
            .filter<File> { it.name.endsWith("csv") }
            .handle(MessageHandler {
                println((it.payload as File).readText())
            }).get()
}