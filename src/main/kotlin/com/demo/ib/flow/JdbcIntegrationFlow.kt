package com.demo.ib.flow

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.Pollers
import org.springframework.integration.dsl.SourcePollingChannelAdapterSpec
import org.springframework.integration.dsl.StandardIntegrationFlow
import org.springframework.integration.file.FileWritingMessageHandler
import org.springframework.integration.jdbc.JdbcPollingChannelAdapter
import java.io.File
import java.util.function.Consumer
import javax.sql.DataSource

@Configuration
class JdbcIntegrationFlow {

    @Bean
    fun integrationFlow(dataSource: DataSource): StandardIntegrationFlow {
        val fileWritingMessageHandler = FileWritingMessageHandler(File("/Users/thegreatdao/Desktop/integration/files"))
        fileWritingMessageHandler.setAutoCreateDirectory(true)
        fileWritingMessageHandler.setAppendNewLine(true)
        fileWritingMessageHandler.setFileNameGenerator {
            System.currentTimeMillis().toString()
        }
        fileWritingMessageHandler.setExpectReply(false)

        return IntegrationFlows
            .from(
                JdbcPollingChannelAdapter(dataSource, "SELECT * FROM Country"),
                Consumer<SourcePollingChannelAdapterSpec> { sourcePollingChannelAdapterSpec ->
                    sourcePollingChannelAdapterSpec.poller(Pollers.fixedRate(1000))
                })
            .transform<List<Map<String, Any>>, String> {
                (it[0].entries.toList()[1].value as String).toUpperCase()
            }
            .handle(fileWritingMessageHandler)
            .get()!!
    }
}