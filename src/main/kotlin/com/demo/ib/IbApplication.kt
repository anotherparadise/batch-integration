package com.demo.ib

import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.integration.config.EnableIntegration

@EnableIntegration
@SpringBootApplication
class IbApplication

fun main(vararg args: String) {
    SpringApplicationBuilder(IbApplication::class.java).web(WebApplicationType.NONE).run(*args)
}